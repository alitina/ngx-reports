export const REPORT_LIST = {
    "model": "ReportTemplates",
    "title": "Report Templates",
    "searchExpression":"indexof(name, '${text}') ge 0 or indexof(reportCategory/name, '${text}') ge 0",
    "columns": [
      {
        "name": "id",
        "title": "ID",
        "hidden": true
      },
      {
        "name": "name",
        "title": "Reports.Attributes.reportTemplate",
        "formatString": "${name}",
        "formatter": "TemplateFormatter"
      },
      {
        "name": "reportCategory/name",
        "property": "reportCategoryName",
        "title": "Reports.Attributes.reportCategory",
        "hidden": true
      },
      {
        "name": "signReport",
        "title": "Reports.Attributes.useDigitalSignature",
        "hidden": true
      },
      {
        "name": "url",
        "title": "Reports.Attributes.useDigitalSignature",
        "hidden": true
      },
      {
        "name": "dateCreated",
        "title": "Forms.DateCreated",
        "hidden": true
      },
      {
        "name": "dateModified",
        "title": "Forms.DateModified",
        "hidden": true
      }
    ],
    "criteria": [
    ]
  }
  