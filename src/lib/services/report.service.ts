import { Injectable } from '@angular/core';
import { AngularDataContext } from '@themost/angular';
import { cloneDeep } from 'lodash';
import { REPORT_PARAMETERS } from './report-parameters';
import { ServiceUrlPreProcessor } from '@universis/forms';
import { HttpClient } from '@angular/common/http';
import { SignerService } from '@universis/ngx-signer';

export declare interface BlobContent {
  contentLocation: string;
  contentLanguage?: string;
  contentEncoding?: string;
  contentDisposition?: string;
}

const formioDefaultCalendarWidgetOptions = {
  'format': 'yyyy-MM-dd',
  'tableView': false,
  'enableMinDateInput': false,
  'datePicker': {
      'disableWeekends': false,
      'disableWeekdays': false
    },
    'enableMaxDateInput': false,
    'enableTime': false,
    'type': 'datetime',
    'input': true,
    'widget': {
      'type': 'calendar',
      'displayInTimezone': 'viewer',
      'locale': 'en',
      'useLocaleSettings': false,
      'allowInput': true,
      'mode': 'single',
      'enableTime': false,
      'noCalendar': false,
      'format': 'yyyy-MM-dd',
      'hourIncrement': 1,
      'minuteIncrement': 1,
      'time_24hr': false,
      'disableWeekends': false,
      'disableWeekdays': false
    }
  };

@Injectable()
export class ReportService {

  public entitySet: any;

  constructor(private _context: AngularDataContext,
    private _http: HttpClient,
    private _signer: SignerService) {
    //
  }

  /**
   * Reads report parameters
   */
  readReport(id: any): Promise<any> {
    return this._context.model(`ReportTemplates/${id}/read`).asQueryable().getItem();
  }
  /**
   * Gets a report template
   * @param {number} id
   */
  getReport(id: any): Promise<any> {
    return this._context.model('ReportTemplates')
      .where('id').equal(id)
      .expand('reportCategory,exportFormats')
      .getItem();
  }

  async getReportFormFor(report: any, options: any) {
    const form = <any> cloneDeep(REPORT_PARAMETERS);
    const columns = form.components[0].components[0].columns;
    new ServiceUrlPreProcessor(this._context).parse(form);
    const requiresUsernamePassword = await this._signer.requiresUsernamePassword();
    // add extra parameters
    const params = {
      // use supplied documentSeriesUrl for getting available document series
      documentSeriesUrl: options.documentSeriesUrl,
      requiresUsernamePassword: requiresUsernamePassword
    };
    form.params = form.params || {};
    Object.assign(form.params, params);
    return form;
  }

  /**
   * Prints a report template
   * @param {number} id
   * @param {*} reportParams
   */
  async printReport(id: number, reportParams: any): Promise<Blob> {
    // todo: pass accept content type based on export type provided by the user
    const headers = new Headers({
      'Accept': 'application/pdf',
      'Content-Type': 'application/json'
    });
    // get service headers
    const serviceHeaders = this._context.getService().getHeaders();
    // manually assign service headers
    Object.keys(serviceHeaders).forEach((key) => {
      if (Object.prototype.hasOwnProperty.call(serviceHeaders, key)) {
        headers.set(key, serviceHeaders[key]);
      }
    });
    // get print url
    const printURL = this._context.getService().resolve(`ReportTemplates/${id}/print`);
    // get report blob
    return this._http.post(printURL, reportParams, {
      headers: this._context.getService().getHeaders(),
      responseType: 'blob',
      observe: 'response'
    }).toPromise().then((response: any) => {
      const contentLocation = response.headers.get('content-location');
      if (contentLocation != null) {
        Object.defineProperty(response.body || new Blob(), 'contentLocation', {
          configurable: true,
          enumerable: true,
          writable: true,
          value: contentLocation
        });
      }
      return response.body;
    });
  }

  async getReportData(id: number): Promise<any> {
    return this._context.model(`ReportTemplates/${id}/read`)
      .getItems();
  }

  /**
   *
   * Maps the input type from the API reports to formio input types
   *
   * @param type The data type as it's defined by the API
   * @returns The formio corresponding formio input type
   *
   */
  mapFieldType(type: string): string {
    switch (type) {
      case 'singleText': return 'textfield';
      case 'bool': return 'checkbox';
      case 'singleSelect':
      case 'multiSelect':
      case 'model': return 'select';
      case 'datetime': return 'datetime';
      default: return 'textfield';
    }
  }


  readSelectDataValues(inputControl: any): any[] {
    if (inputControl && inputControl.state && inputControl.state.options) {
      return inputControl.state.options.map(({ label, value }: any) => ({
        label,
        value
      }));
    } else {
      return [];
    }
  }

  /**
   *
   * Fetches and formats the entity types set from an array to an object
   * with keys the entityType and value the entityType item
   *
   */
  async getEntityTypes(): Promise<any> {
    if (this.entitySet) {
      return this.entitySet;
    }

    const metadata = await this._context.getMetadata();

    this.entitySet = {};
    for (let i = 0; i < metadata.EntityContainer.EntitySet.length; i++) {
      const key = metadata.EntityContainer.EntitySet[i].EntityType;
      this.entitySet[key] = metadata.EntityContainer.EntitySet[i];
    }

    return this.entitySet;
  }

  /**
   *
   * Given a list of a report input control, generates the Form.io components
   * json for the report variables
   *
   * @param inputControls The list of report input controls
   *
   */
  async generateVariablesForm(inputControls: any[]): Promise<any> {
    const entitySet = await this.getEntityTypes();

    inputControls.forEach(inputControl => {
      if (entitySet.hasOwnProperty(inputControl.dataType.label)) {
        inputControl.type = 'model';
      }
    });

    const httpContextBase = this._context.getBase();
    const rawHeaders = this._context.getService().getHeaders();
    const headers = Object.keys(rawHeaders).map(header => ({
      key: header,
      value: rawHeaders[header]
    }));

    const mainColumns = inputControls.map((item) => {
      const type = this.mapFieldType(item.type);
      const description = type !== 'checkbox' ? item.description : undefined;
      const formControl = {} as any;
      if (type === 'datetime') {
        Object.assign(formControl, formioDefaultCalendarWidgetOptions);
      } else if (item.type === 'multiSelect') {
        Object.assign(formControl, {
          multiple: true
        });
      }

      const currentOptions = {
        label: item.label || '',
        description: description,
        tableView: true,
        key: item.id,
        type: type,
        input: true,
        validate: {
          required: item && item.mandatory
        }
      } as any;
      Object.assign(formControl, currentOptions);

      if (item.type === 'model') {
        const entity = entitySet[item.dataType.label];
        formControl.dataSrc = 'url';
        formControl.selectValues = 'value';
        formControl.widget = 'choicesjs';
        formControl.data = {
          // tslint:disable-next-line: max-line-length
          url: `${httpContextBase}${entity.Name}?$top={{limit}}&$orderby=id desc&$skip={{skip}}&$select=id,name&$filter=indexof(name, '{{encodeURIComponent(search||'')}}') ge 0`,
          headers: headers
        };
        formControl.dataType = 'object';
        formControl.idPath = 'id';
        formControl.template = `<span class='d-block pr-5 text-truncate'>{{ item.name }}</span>`;
        formControl.lazyLoad = true;
        formControl.searchField = 'search';
        formControl.limit = 15;
        // tslint:disable-next-line: max-line-length
        if (!item.mandatory) { formControl.calculateValue = 'if(typeof value == \'object\' && Object.keys(value).length === 0 && _.has(data, instance.path)) { _.set(data, instance.path, null)}'; }
      } else if (type === 'select') {
        formControl.data = {
          values: this.readSelectDataValues(item)
        };
      }

      return formControl;
    });

    const formConfig = {
      components: mainColumns
    };
    return formConfig;
  }

  /**
  *
  * Downloads a blob to a file with the specified name
  *
  * @param blob The blob to download.
  * @param {string} fileName The name that the downloaded file should have (including extension).
  *
  */
  downloadBlob(blob: Blob, fileName: string) {
    const objectUrl = window.URL.createObjectURL(blob);
    const a = document.createElement('a');
    document.body.appendChild(a);
    a.setAttribute('style', 'display: none');
    a.href = objectUrl;
    a.download = fileName;
    a.click();
    window.URL.revokeObjectURL(objectUrl);
    a.remove();
  }
}
